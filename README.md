
![](https://github.com/aledc7/react_todo/blob/master/src/images/react2.gif)


  



[![aledc.tk](https://github.com/aledc7/Scrum-Certification/blob/master/recursos/aledc.com.svg)](https://aledc.tk)
[![License](https://github.com/aledc7/Scrum-Certification/blob/master/recursos/mit-license.svg)](https://aledc.tk)
[![GitHub release](https://github.com/aledc7/Scrum-Certification/blob/master/recursos/release.svg)](https://aledc.tk)
[![Dependencies](https://github.com/aledc7/Scrum-Certification/blob/master/recursos/dependencias-none.svg)](https://aledc.tk)

# App Tasks on React JS

[Live Demo](https://aledc7.github.io/react_todo/)

### Features:

- [x] Mobile First
- [x] Dinamic Tasks Counter
- [x] Data Persistence in LocalStorage
- [x] MaterialUI Elements
- [x] React Icons
- [x] Sweet Alert 2
- [x] Forms Input Validation
- [x] Full CRUD
- [x] Max Storage Space: 2.500,000  characters or 5 Megabytes.    
 
_________________________________________________________________________________
#### Install:
1. - Clone the repo
```
git clone https://github.com/aledc7/react_todo/
```
2. - Install dependences
```
npm install
```
3. - Run the App
```
npm start
```
_________________________________________________________________________________

_________________________________________________________________________________


